package prieto.fernando.mytaxiapp.scheduler

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import prieto.fernando.jokesapp.presentation.base.BaseSchedulerProvider

class TestSchedulerProvider : BaseSchedulerProvider() {
    override fun io(): Scheduler = Schedulers.trampoline()
    override fun ui(): Scheduler = Schedulers.trampoline()
    override fun computation(): Scheduler = Schedulers.trampoline()
    override fun newThread(): Scheduler = Schedulers.trampoline()
}
