package prieto.fernando.jokesapp.domain.repository

import com.nhaarman.mockito_kotlin.given
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import prieto.fernando.jokesapp.data.network.local.CategoryLocalModel
import prieto.fernando.jokesapp.data.network.local.RandomJokeLocalModel
import prieto.fernando.jokesapp.data.network.remote.JokeResponse
import prieto.fernando.jokesapp.data.network.remote.RandomJokeResponse
import prieto.fernando.jokesapp.domain.mapper.MultipleRandomJokeResponseToLocalModelMapper
import prieto.fernando.jokesapp.domain.mapper.RandomJokeResponseToLocalModelMapper
import prieto.fernando.jokesapp.domain.source.JokesLocalSource
import prieto.fernando.jokesapp.domain.source.JokesRemoteSource

@RunWith(MockitoJUnitRunner::class)
class JokesRepositoryTest {
    private lateinit var cut: JokesRepository

    @Mock
    lateinit var localSource: JokesLocalSource

    @Mock
    lateinit var remoteSource: JokesRemoteSource

    @Mock
    lateinit var jokeResponseToLocalMapper: RandomJokeResponseToLocalModelMapper

    @Mock
    lateinit var multipleJokesResponseToLocalMapper: MultipleRandomJokeResponseToLocalModelMapper

    @Before
    fun setUp() {
        cut = JokesRepository(
            localSource,
            remoteSource,
            jokeResponseToLocalMapper,
            multipleJokesResponseToLocalMapper
        )
    }

    @Test
    fun `When randomJoke then return RandomJokeLocalModel`() {
        // Given
        val randomJokeResponse = RandomJokeResponse(
            "some Type",
            JokeResponse(
                "some Id",
                "nice joke",
                listOf("explicit")
            )
        )

        val randomJokeLocalModel = RandomJokeLocalModel(
            "some Id",
            "nice joke",
            listOf(CategoryLocalModel.EXPLICIT)
        )

        given { remoteSource.getRandomJoke() }.willReturn(
            Single.just(randomJokeResponse)
        )
        given { jokeResponseToLocalMapper.toLocal(randomJokeResponse) }.willReturn(
            randomJokeLocalModel
        )

        // When
        val actualValue = cut.randomJoke()

        // Then
        actualValue
            .test()
            .assertValue(randomJokeLocalModel)
            .assertNoErrors()
            .assertComplete()
    }

    @Test
    fun `Given input parameters when randomCustomJoke then return RandomJokeLocalModel`() {
        // Given
        val randomJokeResponse = RandomJokeResponse(
            "some Type",
            JokeResponse(
                "some Id",
                "nice joke mentioning firstName lastName",
                listOf("explicit")
            )
        )

        val randomJokeLocalModel = RandomJokeLocalModel(
            "some Id",
            "nice joke mentioning firstName lastName",
            listOf(CategoryLocalModel.EXPLICIT)
        )
        given { remoteSource.getRandomCustomJoke("firstName", "lastName") }.willReturn(
            Single.just(randomJokeResponse)
        )
        given { jokeResponseToLocalMapper.toLocal(randomJokeResponse) }.willReturn(
            randomJokeLocalModel
        )

        // When
        val actualValue = cut.randomCustomJoke("firstName", "lastName")

        // Then
        actualValue
            .test()
            .assertValue(randomJokeLocalModel)
            .assertNoErrors()
            .assertComplete()
    }
}
