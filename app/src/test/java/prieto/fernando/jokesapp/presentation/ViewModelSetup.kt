package prieto.fernando.jokesapp.presentation

import prieto.fernando.jokesapp.presentation.base.BaseViewModel
import prieto.fernando.mytaxiapp.scheduler.TestSchedulerProvider

fun setupViewModelForTests(baseViewModel: BaseViewModel) {
    baseViewModel.schedulerProvider = TestSchedulerProvider()
}
