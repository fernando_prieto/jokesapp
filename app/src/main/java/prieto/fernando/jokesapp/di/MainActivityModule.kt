package prieto.fernando.jokesapp.di

import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import prieto.fernando.jokesapp.presentation.custom.CustomJokeViewModel
import prieto.fernando.jokesapp.presentation.dashboard.DashboardViewModel
import prieto.fernando.jokesapp.presentation.infinite.InfiniteJokesViewModel
import prieto.fernando.jokesapp.presentation.main.MainViewModel
import prieto.fernando.jokesapp.view.MainActivity
import prieto.fernando.jokesapp.view.custom.CustomJokeFragment
import prieto.fernando.jokesapp.view.dashboard.DashboardFragment
import prieto.fernando.jokesapp.view.infinite.InfiniteJokesFragment

@Module
internal abstract class MainActivityModule {
    @ActivityScope
    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun bindDashboardFragment(): DashboardFragment

    @ContributesAndroidInjector
    internal abstract fun bindCustomJokeFragment(): CustomJokeFragment

    @ContributesAndroidInjector
    internal abstract fun bindInfiniteJokesFragment(): InfiniteJokesFragment

    @Module
    companion object {
        @Provides
        @JvmStatic
        internal fun provideMainViewModelFactory(viewModel: MainViewModel): ViewModelProviderFactory<MainViewModel> {
            return ViewModelProviderFactory(viewModel)
        }

        @Provides
        @JvmStatic
        internal fun provideDashboardViewModelFactory(dashboardViewModel: DashboardViewModel): ViewModelProviderFactory<DashboardViewModel> {
            return ViewModelProviderFactory(dashboardViewModel)
        }

        @Provides
        @JvmStatic
        internal fun provideCustomJokeViewModelFactory(customJokeViewModel: CustomJokeViewModel): ViewModelProviderFactory<CustomJokeViewModel> {
            return ViewModelProviderFactory(customJokeViewModel)
        }

        @Provides
        @JvmStatic
        internal fun provideInfiniteJokesViewModelFactory(infiniteJokesViewModel: InfiniteJokesViewModel): ViewModelProviderFactory<InfiniteJokesViewModel> {
            return ViewModelProviderFactory(infiniteJokesViewModel)
        }
    }
}
