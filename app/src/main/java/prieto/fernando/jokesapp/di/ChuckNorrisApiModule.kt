package prieto.fernando.jokesapp.di

import dagger.Module
import dagger.Provides
import javax.inject.Singleton
import okhttp3.OkHttpClient
import prieto.fernando.jokesapp.data.network.ApiService
import prieto.fernando.jokesapp.domain.mapper.MultipleRandomJokeResponseToLocalModelMapper
import prieto.fernando.jokesapp.domain.mapper.RandomJokeResponseToLocalModelMapper
import prieto.fernando.jokesapp.domain.repository.JokesRepository
import prieto.fernando.jokesapp.domain.source.JokesLocalSource
import prieto.fernando.jokesapp.domain.source.JokesRemoteSource
import retrofit2.Retrofit

@Module
class ChuckNorrisApiModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        @Singleton
        internal fun provideChuckNorrisApi(retrofit: Retrofit) =
            retrofit.create(ApiService::class.java)

        @Provides
        @JvmStatic
        @Singleton
        internal fun provideChuckNorrisRetrofit(
            httpBuilder: OkHttpClient.Builder,
            retrofitBuilder: Retrofit.Builder
        ) = retrofitBuilder
            .client(httpBuilder.build())
            .build()

        @Provides
        fun provideRandomJokeResponseToLocalModelMapperRepository(
            localSource: JokesLocalSource,
            remoteSource: JokesRemoteSource,
            jokeResponseToLocalMapper: RandomJokeResponseToLocalModelMapper,
            multipleJokesResponseToLocalMapper: MultipleRandomJokeResponseToLocalModelMapper
        ): JokesRepository =
            JokesRepository(
                localSource,
                remoteSource,
                jokeResponseToLocalMapper,
                multipleJokesResponseToLocalMapper
            )
    }
}
