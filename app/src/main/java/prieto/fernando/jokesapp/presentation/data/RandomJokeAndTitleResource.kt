package prieto.fernando.jokesapp.presentation.data

data class RandomJokeAndTitleResource(
    val randomJokeUiModel: RandomJokeUiModel,
    val titleResource: Int
)
