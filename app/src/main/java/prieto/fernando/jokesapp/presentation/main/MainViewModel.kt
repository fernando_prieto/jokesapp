package prieto.fernando.jokesapp.presentation.main

import android.app.Application
import javax.inject.Inject
import prieto.fernando.jokesapp.presentation.base.BaseViewModel
import prieto.fernando.jokesapp.presentation.base.BaseViewModelInputs
import prieto.fernando.jokesapp.presentation.base.BaseViewModelOutputs

interface MainViewModelInputs : BaseViewModelInputs

interface MainViewModelOutputs : BaseViewModelOutputs

open class MainViewModel @Inject constructor(application: Application) : BaseViewModel(application),
    MainViewModelInputs,
    MainViewModelOutputs {

    override val inputs: MainViewModelInputs
        get() = this

    override val outputs: MainViewModelOutputs
        get() = this
}
