package prieto.fernando.jokesapp.domain.usecase

import dagger.Reusable
import javax.inject.Inject
import prieto.fernando.jokesapp.domain.repository.JokesRepository

@Reusable
class ResetCustomRandomJokeUseCase @Inject constructor(
    private val jokesRepository: JokesRepository
) {
    fun execute() = jokesRepository.resetCustomRandomJoke()
}
