package prieto.fernando.jokesapp.domain.repository

import dagger.Reusable
import io.reactivex.Single
import javax.inject.Inject
import prieto.fernando.jokesapp.domain.mapper.MultipleRandomJokeResponseToLocalModelMapper
import prieto.fernando.jokesapp.domain.mapper.RandomJokeResponseToLocalModelMapper
import prieto.fernando.jokesapp.domain.source.JokesLocalSource
import prieto.fernando.jokesapp.domain.source.JokesRemoteSource

@Reusable
class JokesRepository @Inject constructor(
    private val localSource: JokesLocalSource,
    private val remoteSource: JokesRemoteSource,
    private val jokeResponseToLocalMapper: RandomJokeResponseToLocalModelMapper,
    private val multipleJokeResponseToLocalMapper: MultipleRandomJokeResponseToLocalModelMapper
) {
    fun randomJoke() = remoteSource.getRandomJoke().map { response ->
        jokeResponseToLocalMapper.toLocal(response)
    }

    fun randomCustomJoke(
        firstName: String?,
        lastName: String?
    ) = if (firstName.isNullOrBlank() || lastName.isNullOrBlank()) {
        retrieveLocalJokeOrException()
    } else {
        remoteSource.getRandomCustomJoke(firstName, lastName).map { response ->
            val randomJokeLocalModel = jokeResponseToLocalMapper.toLocal(response)
            localSource.setCustomJoke(randomJokeLocalModel)
            randomJokeLocalModel
        }
    }

    fun multipleRandomJokes(
        numberOfJokes: Int
    ) = remoteSource.getMultipleRandomJoke(numberOfJokes).map { response ->
        multipleJokeResponseToLocalMapper.toLocal(response)
    }

    fun resetCustomRandomJoke() = localSource.resetData()

    private fun retrieveLocalJokeOrException() =
        if (localSource.hasCustomJokesValidData()) {
            localSource.getCustomJoke()
        } else {
            Single.error(CustomJokesException())
        }
}

class CustomJokesException : Exception("There are no jokes saved in cache")
