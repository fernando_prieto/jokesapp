package prieto.fernando.jokesapp.domain.source

import dagger.Reusable
import javax.inject.Inject
import prieto.fernando.jokesapp.data.network.ApiService

@Reusable
class JokesRemoteSource
@Inject constructor(
    private val apiService: ApiService
) {
    fun getRandomJoke() = apiService.getRandomJoke()

    fun getRandomCustomJoke(firstName: String, lastName: String) =
        apiService.getRandomCustomJoke(firstName, lastName)

    fun getMultipleRandomJoke(numberOfJokes: Int) =
        apiService.getMultipleRandomJoke(numberOfJokes)
}
