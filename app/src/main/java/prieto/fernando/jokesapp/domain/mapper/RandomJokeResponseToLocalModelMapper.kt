package prieto.fernando.jokesapp.domain.mapper

import dagger.Reusable
import javax.inject.Inject
import prieto.fernando.jokesapp.data.network.local.CategoryLocalModel
import prieto.fernando.jokesapp.data.network.local.RandomJokeLocalModel
import prieto.fernando.jokesapp.data.network.remote.RandomJokeResponse

@Reusable
class RandomJokeResponseToLocalModelMapper @Inject constructor() {
    fun toLocal(randomJokeResponse: RandomJokeResponse) =
        RandomJokeLocalModel(
            id = randomJokeResponse.value.id,
            joke = randomJokeResponse.value.joke,
            categories = getLocalCategories(randomJokeResponse.value.categories)
        )

    private fun getLocalCategories(categories: List<String>) =
        categories.map { category ->
            when (category) {
                "explicit" -> CategoryLocalModel.EXPLICIT
                "nerdy" -> CategoryLocalModel.NERDY
                else -> CategoryLocalModel.UNKNOWN
            }
        }
}
