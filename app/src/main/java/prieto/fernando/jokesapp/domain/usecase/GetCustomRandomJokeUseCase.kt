package prieto.fernando.jokesapp.domain.usecase

import dagger.Reusable
import javax.inject.Inject
import prieto.fernando.jokesapp.domain.mapper.RandomJokeLocalToDomainModelMapper
import prieto.fernando.jokesapp.domain.repository.JokesRepository

@Reusable
class GetCustomRandomJokeUseCase @Inject constructor(
    private val jokesRepository: JokesRepository,
    private val randomJokeToDomainMapper: RandomJokeLocalToDomainModelMapper
) {
    fun execute(firstName: String?, lastName: String?) =
        jokesRepository.randomCustomJoke(firstName, lastName)
            .map { randomJokeLocalModel ->
                randomJokeToDomainMapper.toDomain(randomJokeLocalModel)
            }
}
