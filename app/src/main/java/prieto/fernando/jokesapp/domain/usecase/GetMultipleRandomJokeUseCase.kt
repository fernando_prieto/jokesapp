package prieto.fernando.jokesapp.domain.usecase

import dagger.Reusable
import io.reactivex.Single
import javax.inject.Inject
import prieto.fernando.jokesapp.domain.mapper.RandomJokeLocalToDomainModelMapper
import prieto.fernando.jokesapp.domain.repository.JokesRepository

@Reusable
class GetMultipleRandomJokeUseCase @Inject constructor(
    private val jokesRepository: JokesRepository,
    private val randomJokeToDomainMapper: RandomJokeLocalToDomainModelMapper
) {
    fun execute(numberOfJokes: Int) = jokesRepository.multipleRandomJokes(numberOfJokes)
        .flatMap { randomJokeLocalModels ->
            Single.just(
                randomJokeLocalModels.map { randomJokeLocalModel ->
                    randomJokeToDomainMapper.toDomain(
                        randomJokeLocalModel
                    )
                }
            )
        }
}
