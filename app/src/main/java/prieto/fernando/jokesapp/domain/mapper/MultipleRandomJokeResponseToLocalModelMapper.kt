package prieto.fernando.jokesapp.domain.mapper

import dagger.Reusable
import javax.inject.Inject
import prieto.fernando.jokesapp.data.network.local.CategoryLocalModel
import prieto.fernando.jokesapp.data.network.local.RandomJokeLocalModel
import prieto.fernando.jokesapp.data.network.remote.MultipleRandomJokeResponse

@Reusable
class MultipleRandomJokeResponseToLocalModelMapper @Inject constructor() {
    fun toLocal(multipleRandomJokeResponse: MultipleRandomJokeResponse) =
        multipleRandomJokeResponse.value.map { multipleJokeResponse ->
            RandomJokeLocalModel(
                id = multipleJokeResponse.id,
                joke = multipleJokeResponse.joke,
                categories = getLocalCategories(multipleJokeResponse.categories)
            )
        }

    private fun getLocalCategories(categories: List<String>) =
        categories.map { category ->
            when (category) {
                "explicit" -> CategoryLocalModel.EXPLICIT
                "nerdy" -> CategoryLocalModel.NERDY
                else -> CategoryLocalModel.UNKNOWN
            }
        }
}
