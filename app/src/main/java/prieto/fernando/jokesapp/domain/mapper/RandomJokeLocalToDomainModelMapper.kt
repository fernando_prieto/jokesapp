package prieto.fernando.jokesapp.domain.mapper

import dagger.Reusable
import javax.inject.Inject
import prieto.fernando.jokesapp.data.network.local.CategoryLocalModel
import prieto.fernando.jokesapp.data.network.local.RandomJokeLocalModel
import prieto.fernando.jokesapp.domain.data.CategoryDomainModel
import prieto.fernando.jokesapp.domain.data.RandomJokeDomainModel

@Reusable
class RandomJokeLocalToDomainModelMapper @Inject constructor() {
    fun toDomain(randomJokeResponse: RandomJokeLocalModel) =
        RandomJokeDomainModel(
            id = randomJokeResponse.id,
            joke = randomJokeResponse.joke,
            categories = getDomainCategories(randomJokeResponse.categories)
        )

    private fun getDomainCategories(categories: List<CategoryLocalModel>) =
        categories.map { category ->
            when (category) {
                CategoryLocalModel.EXPLICIT -> CategoryDomainModel.EXPLICIT
                CategoryLocalModel.NERDY -> CategoryDomainModel.NERDY
                else -> CategoryDomainModel.UNKNOWN
            }
        }
}
