# JokesApp

A demo app to show random jokes, with the aim of showing Clean Architecture and Clean code principles
in a MVVM setup, using RX.

There's a cache memory used to save and retrieve a custom joke. In order to show a data cache implementation.

The implementation was started from the Domain to the View, in order to implement the whole
business logic before the view was presented.

## About the project

The steps which were taken in the implementation were the next ones

1. Dependency Injection implementation ( basic modules Network, Main)
2. API implementation, with DI update
3. Repository implementation with first call randomJoke and test.
4. Repository Mappers and Local entities
5. Domain implementation with test cases and DomainMapper
6. Second call flow customRandomJoke (API,Repository,Domain, tests)
7. Third call flow multipleCustomJokes  (API,Repository,Domain, tests)
8. Presentation Implementation: DashboardViewModel, VM Mapper, refactor
9. Base UI Implementation
10. View Implementation: Dashboard Fragment
11. View + Domain Implementation: Custom Joke
12. Cache implementation for Custom Joke
13. View + Domain Implementation: Infinite Jokes
14. UI Implementation (styling)
15. Beginning of VM tests

